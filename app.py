import os
from flask import Flask
app = Flask(__name__)

@app.route("/")
def main():
    return "Welcome You!"

@app.route('/how are you')
def hello():
    return 'I am good, how about you?'
    
@app.route('/which color')
def colorquest():
    return 'Always green!'

@app.route('/what car')
def carquest():
    return 'Subaru'
    
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
